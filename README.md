# LEAME #

Este es un espejo de instalación para el software xtream ui.

### Como instalo? ###

actualice su ubuntu primero, luego instale el panel  
  
* sudo apt-get update && sudo apt-get upgrade -y; 
* sudo apt-get install libxslt1-dev libcurl3 libgeoip-dev python -y ; 
* rm install.py; wget https://bitbucket.org/emre1393/xtreamui_mirror/downloads/install.py; 
* sudo python install.py  
  
Si desea una instalación completamente NUEVA, elija MAIN.  

Si desea instalar el equilibrio de carga en servidores adicionales, agregue un servidor al panel en la página de administración de servidores, luego ejecute el script y continúe con la opción LB.  

Si desea actualizar el panel de administración, seleccione ACTUALIZAR, luego pegue el enlace de descarga del archivo release_xyz.zip.  


### Archivos Hash ###
* main_xtreamcodes_reborn.tar
* sha1: "532B63EA0FEA4E6433FC47C3B8E65D8A90D5A4E9"

* sub_xtreamcodes_reborn.tar
* sha1: "5F8A7643A9E7692108E8B40D0297A7A5E4423870"

* release_22f.zip
* sha-1: "95471A7EFEB49D7A1F52BAB683EA2BF849F79983"


### nota,
bifurqué este install.py es de https://xtream-ui.com/install/install.py
Por cierto, el desarrollador eliminó la parte de administración de install.py original a principios de este año.
puedes comparar mi install.py con el original.

### nota2,
edite pytools/balancer.py para usar "instalador auto LB" desde este espejo. instalador automático de lb agregado al panel con actualización
`sed -i 's|"https://xtream-ui.com/install/balancer.py"|"https://bitbucket.org/emre1393/xtreamui_mirror/downloads/balancer.py"|g' /home/xtreamcodes/iptv_xtream_codes/pytools/balancer.py`  

### nota3,  
El desarrollador hizo que las versiones de actualización estén abiertas al público después del lanzamiento de r22c, puede descargarlas desde https://xtream-ui.com.
Agregué una parte "ACTUALIZAR" a install.py, pedirá el enlace del archivo zip de actualización.